﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace BEZAOPayDAL.Model
{
    public partial class Transaction
    {
        [Key]
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required]
        [StringLength(10)]
        public string Mode { get; set; }
        [Column(TypeName = "decimal(38, 2)")]
        public decimal Amount { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime Time { get; set; }

        [ForeignKey(nameof(UserId))]
        [InverseProperty("Transactions")]
        public virtual User User { get; set; }
    }
}
