﻿using System;
using System.Linq;
using BEZAOPay3.Models;

namespace BEZAOPay3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("**********Adding new User using EF Core**********");
            AddUser();
            Console.WriteLine("Added Users!");
            Console.WriteLine("**********Updating Existing User using EF Core**********");
            UpdateUser();
            Console.WriteLine("Updated User!");
            Console.WriteLine("**********Getting All Users using EF Core********");
            GetUsers();
            Console.WriteLine("All done fetching!");
            Console.WriteLine("**********Deleting a User using EF Core********");
            DelUser();
            Console.WriteLine("Deleted User !");

            Console.ReadLine();
        }
        //add new user
        static void AddUser()
        {
            try
            {
                using (var user = new BEZAOPayContext())
                {
                    User newUser = new User() { Name = "MikeTyson", Email = "miketyson@domain.com" };
                    User newUser1 = new User() { Name = "MikeTy", Email = "MikeTy@domain.com" };
                    user.Users.Add(newUser);
                    user.Users.Add(newUser1);
                    user.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException?.Message);
            }
        }

        //Update User
        static void UpdateUser()
        {
            try
            {
                using (var user = new BEZAOPayContext())
                {
                    User oldUser = new User() { Id = 2, Name = "Shola", Email = "shola@domain.com" };
                    user.Users.Update(oldUser);
                    user.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException?.Message);
            }
        }

        //Read all Users
        static void GetUsers()
        {
            try
            {
                using (var user = new BEZAOPayContext())
                {
                    foreach (var aUser in user.Users.ToList())
                    {
                        Console.WriteLine($"{aUser.Id}\n{aUser.Name}\n{aUser.Email}\n");
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException?.Message);
            }
        }

        //Delete a User
        static void DelUser()
        {
            try
            {
                using (var user = new BEZAOPayContext())
                {
                    User oldUser = new User() { Id = 6 };
                    user.Users.Remove(oldUser);
                    user.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.InnerException?.Message);
            }
        }
    }
}
