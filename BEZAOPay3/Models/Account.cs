﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAOPay3.Models
{
    class Account
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int Account_Number { get; set; }
        public decimal Balance { get; set; }
        public User User { get; set; }
    }
}
