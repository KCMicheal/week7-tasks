﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAOPay3.Models
{
    class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<Account> Accounts { get; set; }
    }
}
