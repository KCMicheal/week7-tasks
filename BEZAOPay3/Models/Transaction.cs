﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BEZAOPay3.Models
{
    class Transaction
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string Mode { get; set; }
        public int Amount { get; set; }
        public DateTime Time { get; set; }
    }
}
