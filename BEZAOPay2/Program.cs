﻿using BEZAOPay2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BEZAOPay2
{
    class Program
    {
        static void Main(string[] args)
        {
            int userId = AddNewRecord();
            Console.WriteLine(userId);
            Console.ReadLine();
        }

        public static int AddNewRecord()
        {
            using (var context = new BEZAOPayEntities())
            {
                try
                {
                    var newUser = new User() { Name = "MikeTyson", Email = "miketyson@domain.com" };
                    context.Users.Add(newUser);
                    context.SaveChanges();
                    return newUser.Id;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException?.Message);
                    return 0;
                }
            }
        }
    }
}
